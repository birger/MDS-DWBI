#!/usr/bin/python3
# Copyright 2022 by Birger Schacht <birger@rantanplan.org>
# All rights reserved.
# This file is released under the "MIT License".
# Please see https://spdx.org/licenses/MIT.html

# # pylint: disable=C0301, C0103

"""
preprocess.py:
iterate over rows of an csv file and fetch additional information about
geoinformation
"""

import datetime
import json
import csv
import sys
import pathlib
import requests

LOCATIONS = {}
LOCFILE = pathlib.Path('locations.json')
OUTFILE = pathlib.Path('citybike-tripdata.csv')
API_ENDPOINT = "https://nominatim.openstreetmap.org/search.php"

if LOCFILE.exists():
    LOCATIONS = json.loads(LOCFILE.read_text())

def lookup_location(lat, long, rid):
    """
    lookup the location given by longitude and latitude
    it first looks in the LOCATIONS dict for the location
    if it does not find the location there, a lookup is performed
    using get_display_name(lat, long, rid) and the result ist
    added to LOCATIONS and the dict is then saved to a file
    """
    if lat and long:
        locstring = f"{lat},{long}"
        if locstring not in LOCATIONS:
            LOCATIONS[locstring] = get_display_name(lat, long, rid)
            LOCFILE.write_text(json.dumps(LOCATIONS, indent=3))
        return LOCATIONS[locstring]
    return None

def get_display_name(lat, long, rid):
    """
    fetch information from nominatim and
    return the display name of a location
    """
    resp = requests.get(f"{API_ENDPOINT}?q={lat},{long}&format=json")
    if resp:
        json_response = resp.json()
        if json_response:
            return json_response[0]['display_name']
    print(f"Got no usable response for latitude {lat} and longitude {long} for ride id {rid}")
    return None

def get_details_from_display_name(display_name):
    """
    split up a display name and return some subelements
    display_name is in the following format:
    building_name, building_nr, street_name, city, county, state, state_nr, nation
    for example:
    Mis Sueno Polleria, 665, 4th Street, Hoboken, Hudson County, New Jersey, 07030, United States
    or
    Citibike, 12th Street, Hoboken, Hudson County, New Jersey, 07030, United States
    """
    print(display_name)
    street_name, county, city = None, None, None
    if display_name is not None:
        name_list = display_name.split(',')
        name_list = name_list[:-3]
        county = name_list.pop().strip()
        city = name_list.pop().strip()
        if name_list:
            street_name = name_list.pop().strip()
    return (street_name, county, city)

def round_coord(coordinate):
    """
    round the coordinates to 5 decimal places
    """
    return round(float(coordinate), 5) if coordinate else None

def split_date(dtstring):
    """
    extract month, quarter and year from a date object
    """
    dstr = dtstring.split(' ')[0]
    dobj = datetime.datetime.strptime(dstr, "%Y-%m-%d")
    return dobj.month, (dobj.month-1)//3, dobj.year

if len(sys.argv) < 2:
    sys.exit(1)

filenames=sys.argv[1:]
filepaths = [pathlib.Path(filename) for filename in filenames]

with OUTFILE.open('w') as o:
    outfilewriter = csv.writer(o)
    header = [
            'ride_id',
            'rideable_type',
            'started_month',
            'started_quarter',
            'started_year',
            'ended_month',
            'ended_quarter',
            'ended_year',
            'start_lat',
            'start_lng',
            'start_streetname',
            'start_county',
            'start_city',
            'end_lat',
            'end_lng',
            'end_streetname',
            'end_county',
            'end_city'
    ]
    outfilewriter.writerow(header)


for file in filepaths:
    with file.open() as f:
        with OUTFILE.open('a') as o:
            outfilewriter = csv.writer(o)
            rides = csv.DictReader(f)
            for row in rides:
                ride_id = row['ride_id']
                rideable_type = row['rideable_type']
                started_at = row['started_at']
                started_month, started_quarter, started_year = split_date(started_at)
                ended_at = row['ended_at']
                ended_month, ended_quarter, ended_year = split_date(ended_at)
                # the lat and long values are rounded to 5 decimal places, which
                # corresponds to 1.11 meters, which is exact enough for our needs.
                # see https://stackoverflow.com/questions/15965166/what-are-the-lengths-of-location-coordinates-latitude-and-longitude
                start_lat = round_coord(row['start_lat'])
                start_lng = round_coord(row['start_lng'])
                end_lat = round_coord(row['end_lat'])
                end_lng = round_coord(row['end_lng'])
                start_location = lookup_location(start_lat, start_lng, ride_id)
                end_location = lookup_location(end_lat, end_lng, ride_id)
                start_streetname, start_county, start_city = get_details_from_display_name(start_location)
                end_streetname, end_county, end_city = get_details_from_display_name(end_location)
                row = [
                    ride_id,
                    rideable_type,
                    started_month,
                    started_quarter,
                    started_year,
                    ended_month,
                    ended_quarter,
                    ended_year,
                    start_lat,
                    start_lng,
                    start_streetname,
                    start_county,
                    start_city,
                    end_lat,
                    end_lng,
                    end_streetname,
                    end_county,
                    end_city
                ]
                outfilewriter.writerow(row)
