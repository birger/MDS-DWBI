#!/usr/bin/python3
# Copyright 2022 by Birger Schacht <birger@rantanplan.org>
# All rights reserved.
# This file is released under the "MIT License".
# Please see https://spdx.org/licenses/MIT.html

# # pylint: disable=C0301, C0103

"""
loading.py:
load an input file and create dimensional tables and a fact table
based on the information from the input file rows
"""

import csv
import pathlib

INFILE = pathlib.Path('citybike-tripdata.csv')
DIM_BIKETYPE_CSV = pathlib.Path('dim_biketype.csv')
DIM_LOCATION_CSV = pathlib.Path('dim_location.csv')
DIM_DATE_CSV = pathlib.Path('dim_date.csv')
FACT_RIDES_CSV = pathlib.Path('fact_rides.csv')

with INFILE.open('r') as infile, DIM_BIKETYPE_CSV.open('w') as dim_biketype_csv, DIM_LOCATION_CSV.open('w') as dim_location_csv, DIM_DATE_CSV.open('w') as dim_date_csv, FACT_RIDES_CSV.open('w') as fact_rides_csv:
    rides = csv.DictReader(infile)

    dim_biketype_fieldnames = ['business_key', 'primary_key', 'bike_type']
    dim_biketype_writer = csv.DictWriter(dim_biketype_csv, fieldnames=dim_biketype_fieldnames)
    dim_biketype_keys = {}

    dim_location_fieldnames = ['business_key', 'primary_key', 'lat', 'lng', 'city']
    dim_location_writer = csv.DictWriter(dim_location_csv, fieldnames=dim_location_fieldnames)
    dim_location_keys = {}

    dim_date_fieldnames = ['business_key', 'primary_key', 'month', 'quarter', 'year']
    dim_date_writer = csv.DictWriter(dim_date_csv, fieldnames=dim_date_fieldnames)
    dim_date_keys = {}

    fact_rides_fieldnames = ['ride_id', 'biketype_id', 'start_location_id', 'end_location_id', 'start_date_id', 'end_date_id']
    fact_rides_writer = csv.DictWriter(fact_rides_csv, fieldnames=fact_rides_fieldnames)
    fact_rides_ids = set()

    dim_biketype_writer.writeheader()
    dim_location_writer.writeheader()
    dim_date_writer.writeheader()
    fact_rides_writer.writeheader()

    rowcounter = 1
    for row in rides:
        print(rowcounter)
        rowcounter = rowcounter + 1

        biketype_businesskey = row['rideable_type']
        if dim_biketype_keys.get(biketype_businesskey, None) is None:
            dim_biketype_keys[biketype_businesskey] = len(dim_biketype_keys) + 1
            dim_row = {'business_key': biketype_businesskey, 'primary_key': dim_biketype_keys[biketype_businesskey], 'bike_type': row['rideable_type']}
            dim_biketype_writer.writerow(dim_row)

        start_location_businesskey = row['start_lat'] + row['start_lng']
        if dim_location_keys.get(start_location_businesskey, None) is None:
            dim_location_keys[start_location_businesskey] = len(dim_location_keys) + 1
            dim_row = {'business_key': start_location_businesskey, 'primary_key': dim_location_keys[start_location_businesskey], 'lat': row['start_lat'], 'lng': row['start_lng'], 'city': row['start_city']}
            dim_location_writer.writerow(dim_row)

        end_location_businesskey = row['end_lat'] + row['end_lng']
        if dim_location_keys.get(end_location_businesskey, None) is None:
            dim_location_keys[end_location_businesskey] = len(dim_location_keys) + 1
            dim_row = {'business_key': end_location_businesskey, 'primary_key': dim_location_keys[end_location_businesskey], 'lat': row['start_lat'], 'lng': row['start_lng'], 'city': row['start_city']}
            dim_location_writer.writerow(dim_row)

        start_date_businesskey = row['started_year'] + row['started_month']
        if dim_date_keys.get(start_date_businesskey, None) is None:
            dim_date_keys[start_date_businesskey] = len(dim_date_keys) + 1
            dim_row = {'business_key': start_date_businesskey, 'primary_key': dim_date_keys[start_date_businesskey], 'month': row['started_month'], 'quarter': row['started_quarter'], 'year': row['started_year']}
            dim_date_writer.writerow(dim_row)

        end_date_businesskey = row['ended_year'] + row['ended_month']
        if dim_date_keys.get(end_date_businesskey, None) is None:
            dim_date_keys[end_date_businesskey] = len(dim_date_keys) + 1
            dim_row = {'business_key': end_date_businesskey, 'primary_key': dim_date_keys[end_date_businesskey], 'month': row['ended_month'], 'quarter': row['ended_quarter'], 'year': row['ended_year']}
            dim_date_writer.writerow(dim_row)

        ride_id = row['ride_id']
        if ride_id not in fact_rides_ids:
            fact_rides_ids.add(ride_id)
            fact_row = {
                'ride_id': ride_id,
                'biketype_id': dim_biketype_keys[biketype_businesskey],
                'start_location_id': dim_location_keys[start_location_businesskey],
                'end_location_id': dim_location_keys[end_location_businesskey],
                'start_date_id': dim_date_keys[start_date_businesskey],
                'end_date_id': dim_date_keys[end_date_businesskey],
            }
            fact_rides_writer.writerow(fact_row)
